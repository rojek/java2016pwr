package pl.edu.pwr.hello;

import static java.time.Month.DECEMBER;
import static java.time.Month.MAY;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class HoroskopTest {

	private Horoskop horoscope;
	
	@Before
	public void setUp(){
		horoscope = new Horoskop();
	}

	@Test
	public void shouldReturn3OnHealthIndexWhenNameGivenIsMarcinOwczarek() throws Exception {
		
		//given
		String name = "Marcin Owczarek";
		
		//when
		int result = horoscope.getHealthIndex(name);
		
		//then
		assertThat(result, is(equalTo(3)));
	}
	
	@Test
	public void shouldReturn8OnHealthIndexWhenNameGivenIsUlrichVonJungingen() throws Exception {
		
		//given
		String name = "Ulrich von Jungingen";
		
		//when
		int result = horoscope.getHealthIndex(name);
		
		//then
		assertThat(result, is(equalTo(8)));
	}
	
	@Test
	public void shouldReturn8OnLoveIndexAtDayOurTaskIsChecked() throws Exception {
		
		//given
		LocalDate today = LocalDate.of(2016, MAY, 9);
		
		//when
		int result = horoscope.getLoveIndex(today);
		
		//then
		assertThat(result, is(equalTo(8)));
	}
	
	@Test
	public void shouldReturn6OnLoveIndexOnNewYearsEve() throws Exception {
		
		//given
		LocalDate newYearsEve = LocalDate.of(2016, DECEMBER, 31);
		
		//when
		int result = horoscope.getLoveIndex(newYearsEve);
		
		//then
		assertThat(result, is(equalTo(6)));
	}
	
	@Test
	public void shouldReturnIndexLessThan12OnJobIndex() throws Exception {
		
		//given
				
		//when
		int result = horoscope.getJobIndex();
		//then
		assertThat(result, lessThan(12));
	}
	
}