package pl.edu.pwr.hello;

public class HoroskopOutput {
	private String healthMessage;
	private String loveMessage;
	private String jobMessage;
	
	
	public String getHealthMessage() {
		return healthMessage;
	}
	public void setHealthMessage(String healthMessage) {
		this.healthMessage = healthMessage;
	}
	public String getLoveMessage() {
		return loveMessage;
	}
	public void setLoveMessage(String loveMessage) {
		this.loveMessage = loveMessage;
	}
	public String getJobMessage() {
		return jobMessage;
	}
	public void setJobMessage(String jobMessage) {
		this.jobMessage = jobMessage;
	}
	
	@Override
	public String toString() {
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("Zdrowie \n")
				.append(healthMessage)
				.append("\n Miłość \n")
				.append(loveMessage)
				.append("\n Praca \n")
				.append(jobMessage);
		
		return builder.toString();
	}
}
