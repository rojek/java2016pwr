package pl.edu.pwr.hello;

import static pl.edu.pwr.hello.ConsoleParser.parseName;

public class Main {

	public static void main(String[] args) {
		
		String name = parseName();
		Horoskop horoscope = new Horoskop();
		HoroskopOutput output = horoscope.generateHoroscope(name);
		
		System.out.println(output);
	}
}
