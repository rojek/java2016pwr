package pl.edu.pwr.hello;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class ConsoleParser {

	public static String parseName() {

		String name = "";

		Random random = new Random();

		try (BufferedReader bufferRead = new BufferedReader(
				new InputStreamReader(System.in))) {

			System.out.println("Podaj imię:");
			name = bufferRead.readLine();

		} catch (IOException e) {

			System.out.println("Wystąpił wyjątek, generuje losowy ciąg znaków");
			StringBuffer buffer = new StringBuffer();

			for (int i = 0; i < random.nextInt(12); i++) { // zalezy nam tylko
															// na dlugosci
				buffer.append(i);
			}
			name = buffer.toString();
			
		}
		
		return name;
	}
}
