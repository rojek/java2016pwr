package pl.edu.pwr.hello;

import static java.time.LocalDate.now;
import static pl.edu.pwr.hello.Przepowiednie.MILOSC;
import static pl.edu.pwr.hello.Przepowiednie.PRACA;
import static pl.edu.pwr.hello.Przepowiednie.ZDROWIE;

import java.time.LocalDate;
import java.util.Random;

public class Horoskop {

	private static final Random random = new Random();


	public HoroskopOutput generateHoroscope(String name){
		
		HoroskopOutput output = new HoroskopOutput();
		
		output.setHealthMessage(ZDROWIE[getHealthIndex(name)]);
		output.setJobMessage(PRACA[getJobIndex()]);
		output.setLoveMessage(MILOSC[getLoveIndex(now())]);

		return output;
	}
		
	
	public int getHealthIndex(String name) {
		return name.length() % 12;
	}

	public int getLoveIndex(LocalDate date) {
		return (date.getDayOfMonth() - 1) % 12;
	}

	public int getJobIndex() {
		return random.nextInt(12);
	}

}
